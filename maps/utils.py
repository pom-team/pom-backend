import csv
from datetime import date, datetime 
from dateutil.relativedelta import relativedelta

from django.core.files import File
from django.contrib.gis.geos import Polygon, Point

from .models import Sheet, Layer, Locality, Source, Author

def strtobool(str):
    if str == '':
        return None
    else:
        return bool(int(str))

def strtodate(str):
    #import ipdb; ipdb.set_trace()
    if str == '0' or str == '':
        return None
    else:
        try:
            return datetime.strptime(str, '%d-%m-%y') - century
        except ValueError:
            return date(int(str), 1, 1)

def import_sheets(csv_file, layer_id=None):
    read = csv.reader(open(csv_file))
    read.next() # skip the header
    if layer_id:
        layer, created = Layer.objects.get_or_create(id=layer_id) 
    for row in read:
        print(row[11],row[9],row[10], row[8])
        # import ipdb; ipdb.set_trace()
        layer, created = Layer.objects.get_or_create(name=row[0]) 
        sheet, created = Sheet.objects.get_or_create(file_name=row[0], 
                                                     defaults={'layer':layer,
                                                               'sheet_number':row[4],
                                                               'name':row[1],
                                                               'file_name':row[2],
                                                               'year':date(int(row[7]),1,1) if row[4] else None,
                                                               'dropbox_link':row[6],
                                                     })
        if row[11]:
            boundaries = Polygon.from_bbox((row[11],row[9],row[10], row[8])) # Create bounding box, west, south, east, north
            boundaries.srid = 28191 # Set the srid to Palestine Grid
            boundaries.transform(4326) # Transform to WGS84
            sheet.boundaries = boundaries


def import_localities(csv_file):
    localities = csv.reader(open(csv_file, 'r'))
    localities.next()
    century = relativedelta(years=100)
    for row in localities:
        print row[1]
        centroid = Point(float(row[10]), float(row[11]))
        sources_list = row[12].split(',')
        sources = [Source.objects.get_or_create(name=name)[0] for name in sources_list]
        locality_info = {
            'name': row[1],
            'destroyed': strtobool(row[2]),
            'built_over': strtobool(row[3]),
            'reappropriated': strtobool(row[4]),
            'threatened': strtobool(row[5]),
            'refugee_camp': strtobool(row[6]),
            'illegal_settlement': strtobool(row[7]),
            'establishment_date': strtodate(row[8]),
            'depopulation_date': strtodate(row[9]),
            'centroid': centroid,
            'comment': row[13],
            'end_date': strtodate(row[14]),
            'change_status_since_1945': row[15],
        }
        locality, created = Locality.objects.get_or_create(pom_id=row[0], defaults=locality_info)
        locality.sources = sources 
        locality.save()

def import_layers(csv_file):
    layers = csv.reader(open(csv_file, 'r'))
    layers.next()
    for row in layers:
        print(row[6],row[4],row[5], row[3])

        sources = [Source.objects.get_or_create(name=row[11])[0],]
        author, created = Author.objects.get_or_create(name=row[13])
        layer_info = {
            'description': row[1],
            'scale': row[8],
            'start_year': strtodate(row[9]),
            'end_year': strtodate(row[10]),
            'author': author,
        }
        layer, created = Layer.objects.get_or_create(name=row[0], defaults=layer_info)
        layer.sources = sources

        if row[6]:
            boundaries = Polygon.from_bbox((row[6],row[4],row[5], row[3])) # west, south, east, north
            boundaries.srid = int(row[7]) # Set the srid to Palestine Grid
            boundaries.transform(4326) # Transform to WGS84
            layer.boundaries = boundaries

        layer.save()