from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from .views import LocalityViewSet, SheetViewSet, LayerViewSet, basic_map

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'localities', LocalityViewSet)
router.register(r'sheets', SheetViewSet)
router.register(r'layers', LayerViewSet)

# The API URLs are now determined automatically by the router.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^map/$', basic_map, name='basic_map'),
]