from rest_framework_gis import serializers
from rest_framework.serializers import ModelSerializer

from .models import Locality, Sheet, Layer


class LocalitySerializer(ModelSerializer):
    class Meta:
        model = Locality
        fields = '__all__'
        geo_field = 'centroid'

class LocalitySearchSerializer(ModelSerializer):
    class Meta:
        model = Locality
        fields = ['id', 'name', 'centroid', 'change_status_since_1945']
        geo_field = 'centroid'


class SheetSerializer(ModelSerializer):
    class Meta:
        model = Sheet
        fields = '__all__'
        geo_field = 'boundaries'


class LayerSerializer(ModelSerializer):
    class Meta:
        model = Layer
        exclude = ['published', ]
        geo_field = 'boundaries'