from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.contrib.gis.geos import Point

from rest_framework import viewsets, filters

from .models import Locality, Sheet, Layer
from .serializers import LocalitySerializer, LocalitySearchSerializer, SheetSerializer, LayerSerializer

def basic_map(request):
    context = {'hello': 'Hello, world!'}
    return render(request, 'maps/map.html', context)


class LocalityViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Locality.objects.all()
    serializer_class = LocalitySerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name', )

    def get_serializer_class(self):
        if self.action == 'list':
            return LocalitySearchSerializer
        return LocalitySerializer

    def get_queryset(self):
        """
        Optionally restricts the returned localities based on status,
        by filtering against a `status` query parameter in the URL.
        """
        queryset = Locality.objects.all()
        status = self.request.query_params.get('status', None)
        if status is not None:
            queryset = queryset.filter(change_status_since_1945=status)
        return queryset

    @method_decorator(cache_page(60*60*12))
    def dispatch(self, *args, **kwargs):
        return super(LocalityViewSet, self).dispatch(*args, **kwargs)


class SheetViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Sheet.objects.all()
    serializer_class = SheetSerializer

    def get_queryset(self):
        """
        Filter against lat lng params in the URL 
        """
        queryset = Sheet.objects.all()
        lat = self.request.query_params.get('lat', None)
        lng = self.request.query_params.get('lng', None)
        if lat and lng:
            point = Point(float(lng), float(lat))
            queryset = queryset.filter(boundaries__contains=point)
        return queryset

    @method_decorator(cache_page(60*60*12))
    def dispatch(self, *args, **kwargs):
        return super(SheetViewSet, self).dispatch(*args, **kwargs)


class LayerViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Layer.objects.filter(published=True)
    serializer_class = LayerSerializer

    @method_decorator(cache_page(60*60*12))
    def dispatch(self, *args, **kwargs):
        return super(LayerViewSet, self).dispatch(*args, **kwargs)
