from django.contrib import admin

from .models import Layer, Sheet, Locality, Population1945, Population2016, \
                    FurtherReading, Author, Source



class FurtherReadingAdmin(admin.ModelAdmin):
    model = FurtherReading
    exclude = None


class AuthorAdmin(admin.ModelAdmin):
    model = Author
    exclude = None


class SourceAdmin(admin.ModelAdmin):
    model = Source
    exclude = None


class LayerAdmin(admin.ModelAdmin):
    model = Layer
    exclude = None
    list_display = ['name', 'scale', 'published', 'is_default', 'is_overlay', 'is_active', 'order' ]


class SheetAdmin(admin.ModelAdmin):
    model = Sheet
    exclude = ['file',]
    list_display = ['name', 'sheet_number', 'sheet_number_extension', 'year', 'layer']
    list_filter = ['year', 'layer']


class Population1945Inline(admin.StackedInline):
    model = Population1945


class Population2016Inline(admin.StackedInline):
    model = Population2016


class LocalityAdmin(admin.ModelAdmin):
    model = Locality
    exclude = None
    inlines = [Population1945Inline, Population2016Inline]
    list_display = ['name', 'pom_id', 'change_status_since_1945', 'establishment_date', 'depopulation_date', 'end_date']
    list_filter = ['change_status_since_1945', ]

admin.site.register(FurtherReading, FurtherReadingAdmin)
admin.site.register(Author, AuthorAdmin)
admin.site.register(Source, SourceAdmin)
admin.site.register(Layer, LayerAdmin)
admin.site.register(Sheet, SheetAdmin)
admin.site.register(Locality, LocalityAdmin)
