# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-07-21 23:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0024_auto_20180613_1124'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='locality',
            options={'ordering': ['name']},
        ),
        migrations.AddField(
            model_name='layer',
            name='url',
            field=models.URLField(null=True),
        ),
        migrations.AlterField(
            model_name='locality',
            name='change_status_since_1945',
            field=models.CharField(blank=True, choices=[('abandoned', 'Abandoned'), ('depopulated', 'Depopulated'), ('depopulated_built_over', 'Depopulated & built over'), ('depopulated_reappropriated', 'Depopulated & reappropriated'), ('new_locality', 'New locality'), ('remaining', 'Remaining')], max_length=30, null=True),
        ),
    ]
