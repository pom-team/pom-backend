# README #

The repository for the backend of Palestine Open Maps.

### How do I get set up? ###

This is a Django/Mezzanine application. Follow the steps below to get it running locally: 

- Make sure that you have  [`pip`](https://pip.pypa.io/en/stable/installing/) (the Python package manager) installed.
- Using pip Install [`virtualenv`](https://virtualenv.pypa.io/en/stable/installation/) and [`virtualenvwrapper`](https://virtualenvwrapper.readthedocs.io/en/latest/). If on Mac you can use homebrew to install them.
- Configure your shell [`virtualenv wrapper'](http://virtualenvwrapper.readthedocs.io/en/latest/install.html)
- Using `pom` as the name of the virtualenv, create a virtual env:  mkvirtualenv pom
- Activate the virtualenv: `workon pom`
- Clone the repository
- Install the dependencies: `pip install -r requirements.txt`
- Make sure that you have PostgreSQL, PostGIS, and GEOS installed. The easiest way to do that is through [postgress.app](https://postgresapp.com/)
    - create the database with the appropriate GIS extension to the database and the right roles. Follow the instructions [here](https://docs.djangoproject.com/en/2.0/ref/contrib/gis/install/postgis/#post-installation) very closely!
- Create a local_settings.py file, based on [this](https://github.com/stephenmcd/mezzanine/blob/master/mezzanine/project_template/project_name/local_settings.py.template) template. 
    - Make sure that the `DATABASES['default']['ENGINE']` value is set to `django.contrib.gis.db.backends.postgis`. 
- Create the database `python manage.py createdb`
- Load the fixtures: `python manage.py loaddata maps`
- Run the development server `python manage.py runserver`

NB: every time that you pull an update, make sure that you have the latest dependancies and that the database state matches the model state. Run `pip install -r requirements.txt && python manage.py migrate`. Hopefully there won't be any migration conflicts!

### Contribution guidelines ###

* Master branch chould always be deployable!

### Who do I talk to? ###

* hi@majdal.cc is the main author, with  [Visualizing Palestine](https://visualizingpalestine.org/)