from __future__ import unicode_literals

from django.contrib.gis.db import models
from django.utils.translation import gettext as _
from mezzanine.galleries.models import FileField 

POPULATION_GROUP_CHOICES = (
    ('palestinian',_('Palestinian')),
    ('jewish',_('Jewish')),
    ('mixed',_('Mixed')),
)


class Layer(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    boundaries = models.PolygonField(blank=True, null=True)
    scale = models.IntegerField(null=True)

    start_year = models.DateField(blank=True, null=True)
    end_year = models.DateField(blank=True, null=True)

    url = models.URLField(null=True)
    sources = models.ManyToManyField("Source", blank=True)
    attribution = models.CharField(max_length=250, blank=True, null=True)
    further_readings = models.ManyToManyField("FurtherReading", blank=True)
    author = models.ForeignKey("Author", null=True)

    published = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)
    is_default = models.BooleanField(default=False)
    is_overlay = models.BooleanField(default=False)
    order = models.IntegerField(unique=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['order']


class FurtherReading(models.Model):
    name = models.CharField(max_length=100)
    url = models.URLField()

    def __str__(self):
        return self.name


class Author(models.Model):
    name = models.CharField(max_length=100)
    url = models.URLField()
    
    def __str__(self):
        return self.name


class Source(models.Model):
    name = models.CharField(max_length=100)
    url = models.URLField()
    notes = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class Sheet(models.Model):
    layer = models.ForeignKey(Layer, blank=True, null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    file_name = models.CharField(max_length=100)
    file = FileField()
    sheet_number = models.CharField(max_length=5)
    sheet_number_extension = models.CharField(max_length=5)
    dropbox_link = models.URLField()
    year = models.DateField(blank=True, null=True)
    boundaries = models.PolygonField(blank=True, null=True)

    def __str__(self):
        return '{} {}:{} ({})'.format(self.name, self.sheet_number, self.sheet_number_extension, self.layer)

CHANGE_STATUS_SINCE_1945_CHOICES = (
    ('abandoned', _('Abandoned')),
    ('depopulated', _('Depopulated')),
    ('depopulated_built_over',_('Depopulated & built over')), 
    ('depopulated_appropriated', _('Depopulated & appropriated')), 
    ('new_locality', _('New locality')), 
    ('remaining', _('Remaining'))
    ) 

class Locality(models.Model):
    pom_id = models.IntegerField()
    name = models.CharField(max_length=100)
    image = models.ImageField(null=True, blank=True)

    destroyed = models.NullBooleanField()
    built_over = models.NullBooleanField()
    reappropriated = models.NullBooleanField(default=False)

    threatened = models.NullBooleanField(default=False)
    refugee_camp = models.NullBooleanField(default=False)
    illegal_settlement = models.NullBooleanField(default=False)

    establishment_date = models.DateField(blank=True, null=True)
    depopulation_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(null=True, blank=True)

    population_group_1945 = models.CharField(max_length=20, choices=POPULATION_GROUP_CHOICES, null=True, blank=True)
    population_group_2016 = models.CharField(max_length=20, choices=POPULATION_GROUP_CHOICES, null=True, blank=True)

    centroid = models.PointField()

    change_status_since_1945 = models.CharField(max_length=30, choices=CHANGE_STATUS_SINCE_1945_CHOICES, null=True, blank=True)

    sources = models.ManyToManyField("Source")
    comment = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ['name']
        verbose_name = _('Locality')
        verbose_name_plural = _('Localities')

    def __str__(self):
        return self.name

PALESTINIAN_DISTRICT_PRE_48_CHOICES = ()

SIZE_CHOICES = (
    ('2',_('Small')),
    ('4',_('Medium')),
    ('8',_('Large')),
    )


class Population1945(models.Model):
    locality = models.OneToOneField(Locality, on_delete=models.CASCADE)
    palestinian_district = models.CharField(max_length=20, choices=PALESTINIAN_DISTRICT_PRE_48_CHOICES)
    population_group = models.CharField(max_length=20, choices=POPULATION_GROUP_CHOICES)
    population_1922 = models.IntegerField(null=True)
    population_1931 = models.IntegerField(null=True)
    population_1945_palestinian = models.IntegerField(null=True)
    population_1945_jewish = models.IntegerField(null=True)
    population_1945_total = models.IntegerField(null=True)
    size_1945 = models.CharField(max_length=2, choices=SIZE_CHOICES)

    land_area_1945_palestinian = models.IntegerField(null=True)
    land_area_1945_jewish = models.IntegerField(null=True)
    land_area_1945_public = models.IntegerField(null=True)
    
    boundaries = models.PolygonField(null=True)

    source = models.TextField()
    comment = models.TextField()

    def __str__(self):
        return self.locality


PALESTINIAN_DISTRICT_2016_CHOICES = (
    ('bethlehem',_('Bethlehem')),
    ('deir al balah',_('Deir al Balah')),
    ('gaza',_('Gaza')),
    ('hebron',_('Hebron')),
    ('jenin',_('Jenin')),
    ('jericho',_('Jericho')),
    ('jerusalem',_('Jerusalem')),
    ('khan younis',_('Khan Younis')),
    ('nablus',_('Nablus')),
    ('north gaza',_('North Gaza')),
    ('qalqiliya',_('Qalqiliya')),
    ('rafah',_('Rafah')),
    ('ramallah',_('Ramallah')),
    ('salfit',_('Salfit')),
    ('tubas',_('Tubas')),
    ('tulkarm',_('Tulkarm'))
)

ISRAELI_DISTRICT_CHOICES = (
    ('haifa',_('Haifa')),
    ('northern',_('Northern')),
    ('central',_('Central')),
    ('southern',_('Southern')),
    ('jerusalem',_('Jerusalem')),
    ('tel aviv - yafa',_('Tel Aviv - Yafa')),
    ('judea & samaria',_('Judea & Samaria'))
)

class Population2016(models.Model):
    locality = models.OneToOneField(Locality, on_delete=models.CASCADE) 
    palestinian_district = models.CharField(max_length=20, choices=PALESTINIAN_DISTRICT_2016_CHOICES)
    israeli_district = models.CharField(max_length=20, choices=ISRAELI_DISTRICT_CHOICES)
    code = models.IntegerField()
    population_group = models.CharField(max_length=20, choices=POPULATION_GROUP_CHOICES)
    population_palestinian = models.IntegerField()
    population_jewish = models.IntegerField()
    population_other = models.IntegerField()
    population_total = models.IntegerField()

    recognised = models.BooleanField()

    boundaries = models.PolygonField(null=True)

    def __str__(self):
        return self.name