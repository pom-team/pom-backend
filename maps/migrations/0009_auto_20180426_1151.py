# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2018-04-26 11:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0008_auto_20180426_1149'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sheet',
            name='year',
            field=models.DateField(blank=True, null=True),
        ),
    ]
