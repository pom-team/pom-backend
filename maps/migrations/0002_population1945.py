# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2018-04-25 01:49
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Population1945',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('palestinian_district_pre_1948', models.CharField(max_length=20)),
                ('population_group', models.CharField(choices=[('arab', 'Arab'), ('jewish', 'Jewish')], max_length=20)),
                ('population_1922', models.IntegerField(null=True)),
                ('population_1931', models.IntegerField(null=True)),
                ('population_1945_arabs', models.IntegerField(null=True)),
                ('population_1945_jews', models.IntegerField(null=True)),
                ('population_1945_total', models.IntegerField(null=True)),
                ('size_1945', models.CharField(choices=[('2', 'Small'), ('4', 'Medium'), ('8', 'Large')], max_length=2)),
                ('source', models.TextField()),
                ('comment', models.TextField()),
                ('locality', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='maps.Locality')),
            ],
        ),
    ]
